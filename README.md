# Expess-REST-API-example

This is a test REST API using Express, Sequelize, and SQLite

## Getting started and running the project
```
npm i
node app.js
```

## Routes Available

#### GET route: "/" gets all the members
#### GET route: "/:id" gets a specific member
#### POST route: "/create" creates a member
#### PUT  route: "/update/:id" updates a member
#### DELETE route:  "/destroy/:id" deletes a member


